package code.u2764.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.charset.Charset;

public class ConfigFile  {

	private File file;
	
	public ConfigFile(File file) {
		this.file = file;
	}
	
	public Config load() throws IOException, FileNotFoundException {
		final Config config = new Config();
		FileInputStream fin = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fin, Charset.forName("UTF-8")));
		
		String ln;
		
		String comment = "";
		String key;
		String value;
		
		while ((ln = br.readLine()) != null) {
			if (ln.equals("")) continue;
			if (ln.startsWith("#")) {
				if (!comment.equals("")) {
					config.addFileComment(comment.replaceFirst("#", "").trim());
				}
				comment = ln;
			} if (ln.endsWith(": ")) {
				key = ln.replaceAll(": ", "");
				value = "";
				config.set(key, value, comment.replaceFirst("#", "").trim());
				comment = "";
			} else if (ln.contains(": ")) {
				key = ln.split(": ")[0];
				value = ln.split(": ")[1];
				config.set(key, (value != null) ? value : "", comment.replaceFirst("#", "").trim());
				comment = "";
			}
		}
		
		br.close();
		fin.close();
		
		return config;
	}
	
	public void save(Config config) throws FileNotFoundException {
		PrintStream ps = new PrintStream(file);
		
		for (String str : config.fileComments) {
			ps.println("# " + str);
		}
		
		ps.println();
		ps.println();
		ps.println();
		ps.println();
		
		for (String key : config.props.keySet()) {
			Pair<String, String> pair = config.props.get(key);
			if (!pair.comment.equals("")) {
				ps.println("# " + pair.comment);
			}
			ps.println(key + ": " + pair.value);
			ps.println();
		}
		
		ps.close();
	}
	
	public boolean exists() {
		return file.exists();
	}
	
	public void delete() {
		file.delete();
	}
	
}
