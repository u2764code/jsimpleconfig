package code.u2764.config;

public class Pair<V, C> {

	public V value;
	public C comment;
	
	protected Pair(V value, C comment) {
		this.value = value;
		this.comment = comment;
	}

	@Override
	public int hashCode() {
		return value.hashCode() ^ comment.hashCode();
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Pair)) return false;
		Pair<V, C> pair = (Pair<V, C>) o;
		return this.value.equals(pair.value) && this.comment.equals(pair.comment);
	}
	
	
}
