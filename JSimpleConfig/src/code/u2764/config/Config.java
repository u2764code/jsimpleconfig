package code.u2764.config;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class Config {
	
	protected final ArrayList<String> fileComments = new ArrayList<String>();
	protected final LinkedHashMap<String, Pair<String, String>> props = new LinkedHashMap<String, Pair<String, String>>();
	
	public Config() {}
	
	public void addFileComment(String comment) {
		fileComments.add(comment);
	}
	
	public void setPropertyComment(String key, String comment) {
		if (props.containsKey(key)) {
			props.get(key).comment = comment;
		}
	}
	
	public void set(String key, Object value, String comment) {
		final Pair<String, String> pair;
		if (props.containsKey(key)) {
			pair = props.get(key);
			pair.value = value.toString();
		} else {
			pair = new Pair<String, String>(value.toString(), comment);
		}
		props.put(key, pair);
	}
	
	public void set(String key, Object value) {
		set(key, value, "");
	}
	
	public int getInt(String key, int defaultValue) {
		if (!props.containsKey(key)) return defaultValue;
		try {
			return Integer.parseInt(props.get(key).value);
		} catch (NumberFormatException n) {
			n.printStackTrace();
		}
		return defaultValue;
	}
	
	public int getInt(String key) {
		return getInt(key, 0);
	}
	
	public String getString(String key, String defaultValue) {
		if (!props.containsKey(key)) return defaultValue;
		return props.get(key).value;
	}
	
	public String getString(String key) {
		return getString(key, "");
	}
	
	public boolean getBoolean(String key, boolean defaultValue) {
		if (!props.containsKey(key)) return defaultValue;
		return Boolean.parseBoolean(props.get(key).value);
	}
	
	public boolean getBoolean(String key) {
		return getBoolean(key, false);
	}
	
}
